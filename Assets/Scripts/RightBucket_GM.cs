﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightBucket_GM : MonoBehaviour {

	public static RightBucket_GM gmInstance;
	public GameObject respawnLeft, respawnRight;
	public ObjectPooler poolerGreen;
	public int rightGreenBalls;
	public int rightYellowBalls;
	public int rightRedBalls;
	public int rightObjects;
	private float speedSmall = 1.5f;
	private float speedMedium = 1.5f;
	private float speedBig = 1.5f;
	float timeToWait;
	private List<GameObject> objectsInScene;
	private bool canRespawn = true;
	public bool isGameOver = false;
	private Vector2 rangeSmall, rangeMedium, rangeBig;
	
	void Awake(){
		if (gmInstance != null) {
			Destroy (this);
		} else {
			gmInstance = this;
		}
	}
	void Start(){
		objectsInScene = new List<GameObject>();
		createObjects ();
	}
	public void RestartGame(){
		for(int i=0;i< objectsInScene.Count;i++){
			objectsInScene [i].SetActive (false);
			objectsInScene [i].transform.FindChild ("RoundedColor").gameObject.transform.localPosition = new Vector3(0.0199f, -2.5f, 0.0f);
		}
		rightObjects = 0;
		rightGreenBalls = 0;
		rightRedBalls = 0;
		rightYellowBalls = 0;
		SliderTimer.sliderInstance.RestartSlider ();
		canRespawn = true;
		isGameOver = false;
	}
	public void ClearScene(){
		canRespawn = false;
		isGameOver = true;
		for(int i=0;i< objectsInScene.Count;i++){
			objectsInScene [i].SetActive (false);
		}

	}
	void Update(){
		if(canRespawn && !isGameOver){
			canRespawn = false;
			calculateDifficulty ();
			StartCoroutine (TimerSpawnObjects (timeToWait));
		}
	}
	void calculateDifficulty(){
		if(rightObjects >= 0 && rightObjects <= 5){
			timeToWait = Random.Range (2.5f, 5.0f);
			rangeSmall = new Vector2 (2.0f, 3.0f);
			rangeMedium = new Vector2 (2.0f, 2.5f);
			rangeBig = new Vector2 (1.0f, 2.0f);
		}
		if(rightObjects >= 6 && rightObjects <= 10){
			timeToWait = Random.Range (2.5f, 4.5f);
			rangeSmall = new Vector2 (2.3f, 3.3f);
			rangeMedium = new Vector2 (2.3f, 2.8f);
			rangeBig = new Vector2 (1.3f, 2.3f);
		}
		if(rightObjects >= 11 && rightObjects <= 15){
			timeToWait = Random.Range (2.5f, 4.5f);
			rangeSmall = new Vector2 (2.6f, 3.6f);
			rangeMedium = new Vector2 (2.6f, 3.1f);
			rangeBig = new Vector2 (1.6f, 2.6f);

		}
		if(rightObjects >= 16 && rightObjects <= 25){
			timeToWait = Random.Range (1.5f, 3.5f);
			rangeSmall = new Vector2 (2.6f, 3.6f);
			rangeMedium = new Vector2 (2.6f, 3.1f);
			rangeBig = new Vector2 (1.6f, 2.6f);
		}
		if(rightObjects >= 26 && rightObjects <= 40){
			timeToWait = Random.Range (1.0f, 4.5f);
			rangeSmall = new Vector2 (3.0f, 4.0f);
			rangeMedium = new Vector2 (3.0f, 3.9f);
			rangeBig = new Vector2 (2.0f, 3.6f);
		}
		if(rightObjects >= 40){
			timeToWait = Random.Range (1.0f, 3.0f);
		}
	}
	IEnumerator TimerSpawnObjects(float timeToWait){
		yield return new WaitForSeconds(timeToWait);
		createObjects ();
		canRespawn = true;

	}
	public void addSeconds(){
		SliderTimer.sliderInstance.addSeconds (2.0f);
	}
	void createObjects(){
		if(!isGameOver){
			GameObject obj = poolerGreen.GetDesabledObject ();
			int randRespawn = Random.Range (0, 10);
			if (randRespawn % 2 == 0) {
				obj.transform.position = respawnRight.transform.position;
				obj.GetComponent<RightBucket_ObjMovement> ().finalPosition = respawnLeft.transform.position;
			} else {
				obj.transform.position = respawnLeft.transform.position;
				obj.GetComponent<RightBucket_ObjMovement> ().finalPosition = respawnRight.transform.position;	
			}
			obj.SetActive (true);
			obj.GetComponent<RightBucket_ObjMovement> ().canMove = true;
			obj.transform.FindChild("RoundedColor").gameObject.SetActive(true);
			obj.transform.FindChild("RoundedColor").gameObject.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.FreezePositionY;
			//set speed
			if(obj.transform.tag == "ballSmall"){
				float randomSmall = Random.Range (rangeSmall.x, rangeSmall.y);
				obj.GetComponent<RightBucket_ObjMovement> ().speed = randomSmall;
			}
			if(obj.transform.tag == "ballMedium"){
				float randomMedium = Random.Range (rangeMedium.x, rangeMedium.y);
				obj.GetComponent<RightBucket_ObjMovement> ().speed = randomMedium;
			}
			if(obj.transform.tag == "ballBig"){
				float randomBig = Random.Range (rangeBig.x, rangeBig.y);
				obj.GetComponent<RightBucket_ObjMovement> ().speed = randomBig;
			}
			objectsInScene.Add (obj);
		}
	}
}
