﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPooler : MonoBehaviour
{
    [SerializeField]
    GameObject[] pollObject;
    [SerializeField]
    int initialObjectsAmount = 10;
    List<GameObject> objectsPooled;
    int activationIndex = 0;
    public bool willGrow = false;

    private void Start()
    {
		Debug.Log ("Obj pooler started");
        PoolObject();
    }
    /// <summary>
    /// Retorna o primeiro objeto que nao esteja em uso encontrado no pool
    /// </summary>
    /// <returns></returns>
    GameObject GetObject()
    {
        int counter = 0; //Cria novo contador

        for (int i = 0; i < objectsPooled.Count; i++)
        {
            int correctedIndex = activationIndex + counter; //calcula o indice correto de ativação a partir do ultimo ativado
            if (correctedIndex >= objectsPooled.Count) //se o indice for maior que o tamanho da lista, corrige para começar a partir do 0
            {
                correctedIndex = correctedIndex - objectsPooled.Count;
            }

            if (objectsPooled[correctedIndex].activeInHierarchy == false) //se ele nao estiver ativado (nao estiver em uso)
            {
                activationIndex = correctedIndex; //atualiza o activationIndex para o ultimo ativado para que a contegem começe a partir deste na proxima vez
                return objectsPooled[correctedIndex]; //retorna este item
            }

            ++counter; //incrementa o contador
        }

        return ExpandPool(); //Se caso todos estiverem em uso aumenta o tamanho do Pool criando um novo objeto

    }

    public List<GameObject> GetAllPooledObjects()
    {
        return objectsPooled;
    }

    public GameObject GetObjectAtIndex(int index)
    {
        return objectsPooled[index];
    }

    public GameObject GetDesabledObject()
    {
        //print(objectsPooled.Count);

            for (int i = 0; i < objectsPooled.Count; i++)
            {
                if (!objectsPooled[i].activeInHierarchy)
                {
                    return objectsPooled[i];
                }
            }

            if (willGrow)
            {
                int ranNum = Random.Range(0, pollObject.Length);
                GameObject instantiated = GameObject.Instantiate((GameObject)pollObject[ranNum]);
                instantiated.SetActive(false);
                objectsPooled.Add(instantiated);
                return instantiated;

            }
        
        return null;     
    }

    public void PoolObject()
    {
        int ranNum;
        objectsPooled = new List<GameObject>();
        
        for (int i = 0; i <= initialObjectsAmount; i++)
        {
            ranNum = Random.Range(0, pollObject.Length);
            GameObject instantiated = GameObject.Instantiate((GameObject)pollObject[ranNum]);
            instantiated.SetActive(false);
            objectsPooled.Add(instantiated);
        }
        activationIndex = 0;
       
    }

    /// <summary>
    /// Expande o Pool de objetos criando um novo em seu ultimo indice
    /// </summary>
    /// <returns></returns>
    GameObject ExpandPool()
    {
        int ranNum;
        ranNum = Random.Range(0, objectsPooled.Count);
        GameObject instantiated = GameObject.Instantiate((GameObject)pollObject[ranNum]);

        objectsPooled.Add(instantiated);
        instantiated.SetActive(false);
        activationIndex = objectsPooled.Count - 1;

        return objectsPooled[activationIndex];
    }

}
