using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


[RequireComponent(typeof(Slider))]
public class SliderTimer : MonoBehaviour {

    // This Script can be used as TIMER 
    // 
    //  ------------------------------
    //  ||||||||||||||               |
    //  ------------------------------
    // 
    // 1 - CREAT AN SLIDER 
    // 2 - REMOVE HANDLER
    // 3 - SET FILL INITAL AN FINAL POSITION

	public static SliderTimer sliderInstance;
    Slider slider;
    float secondsToGetEmpty = 60;
    
	void Awake(){
		if (sliderInstance != null) {
			Destroy (this);
		} else {
			sliderInstance = this;
		}
	}

    void Start () {
        slider = GetComponent<Slider>();
        slider.maxValue = secondsToGetEmpty;
        slider.minValue = 0;
        slider.value = secondsToGetEmpty;

    }
	public void addSeconds(float seconds){
		slider.value = slider.value + seconds;
	}
	public void RestartSlider(){
		slider.value = secondsToGetEmpty;
	}

    void Update () {
		if (slider.value > 0) {
			slider.value -=  1 * Time.deltaTime;
		}
		if(slider.value <= 0){
			RightBucket_UIManager.uiInstance.IsGameOver ();
		}
    }

}
