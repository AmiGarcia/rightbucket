﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightBucket_RedBucket : MonoBehaviour {


	void OnTriggerEnter2D (Collider2D colisor)
	{
		if (colisor.gameObject.tag == "redBall") {
			RightBucket_GM.gmInstance.rightRedBalls += 1;
			RightBucket_GM.gmInstance.rightObjects += 1;
			RightBucket_UIManager.uiInstance.UpdateUI ();
			colisor.gameObject.SetActive (false);
			colisor.gameObject.transform.localPosition = new Vector3(0.0199f, -2.5f, 0.0f);
			RightBucket_GM.gmInstance.addSeconds ();
		} else {
			colisor.gameObject.SetActive (false);
			colisor.gameObject.transform.localPosition = new Vector3(0.0199f, -2.5f, 0.0f);
		}
	}
}
