﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RightBucket_UIManager : MonoBehaviour {

	public static RightBucket_UIManager uiInstance;

	public GameObject GameOverlay, GameOver;
	public Text rightGreensScore;
	public Text rightYellowScore;
	public Text rightRedScore;
	public Text rightObjects;
	public Text gameOverText;


	void Awake () {
		if (uiInstance != null) {
			Destroy (this);
		} else {
			uiInstance = this;
		}
	}
	void Start(){
		GameOverlay.SetActive (true);
		GameOver.SetActive (false);
	}
	public void UpdateUI(){
		rightGreensScore.text = RightBucket_GM.gmInstance.rightGreenBalls.ToString ();
		rightYellowScore.text = RightBucket_GM.gmInstance.rightYellowBalls.ToString ();
		rightRedScore.text = RightBucket_GM.gmInstance.rightRedBalls.ToString ();
		rightObjects.text = RightBucket_GM.gmInstance.rightObjects.ToString ();
	}
	public void IsGameOver(){
		RightBucket_GM.gmInstance.ClearScene ();
		GameOverlay.SetActive (false);
		GameOver.SetActive (true);
		rightGreensScore.text = "0";
		rightYellowScore.text = "0";
		rightRedScore.text = "0";
		rightObjects.text = "0";
		gameOverText.text = "Você perdeu! Seu número de acertos é: " + RightBucket_GM.gmInstance.rightObjects.ToString ();
	}
	public void RestartGame(){
		Debug.Log ("Restart Game");
		GameOverlay.SetActive (true);
		GameOver.SetActive (false);
		RightBucket_GM.gmInstance.RestartGame ();
	}
}
