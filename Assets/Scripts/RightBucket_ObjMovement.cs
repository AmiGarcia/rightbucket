﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RightBucket_ObjMovement : MonoBehaviour {

	public GameObject ball;
	public bool canMove;
	public float speed = 1.0f;
	public Vector3 finalPosition;

	void Update(){
		if (canMove) {
			transform.position = Vector3.MoveTowards (transform.position, finalPosition, Time.deltaTime * speed);
		}
		if(transform.position == finalPosition){
			gameObject.SetActive (false);
		}
	}
	void OnMouseOver(){
		if(Input.GetMouseButtonDown(0)){
			transform.FindChild("RoundedColor").gameObject.GetComponent<Rigidbody2D> ().constraints = RigidbodyConstraints2D.None;
		}
	}
	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "greenBall" || coll.gameObject.tag == "yellowBall" || coll.gameObject.tag == "redBall") {
			
		}

	}


}
